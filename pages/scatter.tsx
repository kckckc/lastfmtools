import { Box } from '@chakra-ui/react';
import type { NextPage } from 'next';
import { SEO } from '../components/SEO';

const Home: NextPage = () => {
	return (
		<Box m={4}>
			<SEO title="Scatter" description={undefined} />

			<Box id="scatter-div"></Box>
		</Box>
	);
};

export default Home;
