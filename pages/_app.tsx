import { ChakraProvider, extendTheme, Flex } from '@chakra-ui/react';
import type { AppProps } from 'next/app';
import { Sidebar } from '../components/Sidebar';
import '../styles/globals.css';
import { IsHydratedProvider } from '../util/isHydrated';

const theme = extendTheme({
	config: {
		initialColorMode: 'dark',
		useSystemColorMode: false,
	},
});

function MyApp({ Component, pageProps }: AppProps) {
	return (
		<ChakraProvider theme={theme}>
			<IsHydratedProvider>
				<Flex direction="row">
					<Sidebar />
					<Component {...pageProps} />
				</Flex>
			</IsHydratedProvider>
		</ChakraProvider>
	);
}

export default MyApp;
