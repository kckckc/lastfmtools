import { Box, Heading, Link } from '@chakra-ui/react';
import { NextPage } from 'next';
import { SEO } from '../components/SEO';
import NextLink from 'next/link';

const Home: NextPage = () => {
	return (
		<Box m={4}>
			<SEO title="Not found" />

			<Heading size="lg" mb={4}>
				Page not found
			</Heading>
			<Box>
				Click{' '}
				<NextLink href="/" passHref>
					<Link color="blue.400">here</Link>
				</NextLink>{' '}
				to go home.
			</Box>
		</Box>
	);
};

export default Home;
