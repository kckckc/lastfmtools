import { Box, Flex, Heading } from '@chakra-ui/react';
import type { NextPage } from 'next';
import { LinkCard } from '../components/LinkCard';
import { SEO } from '../components/SEO';

const Home: NextPage = () => {
	return (
		<Box m={4} w="100%">
			<SEO />

			<Flex direction="column" h="100%" justifyContent="center" gap={8}>
				<Heading textAlign="center" size="lg">
					Select visualization
				</Heading>
				<Flex
					direction={{ base: 'column', lg: 'row' }}
					w="100%"
					alignItems="center"
					justifyContent="center"
					gap={8}
				>
					<LinkCard
						heading="Chart"
						description="asdf"
						href="/chart"
					/>
					<LinkCard
						heading="Calendar"
						description="asdf"
						href="/calendar"
					/>
				</Flex>
				<Flex direction="row" w="100%" justifyContent="center" gap={8}>
					<LinkCard
						heading="Scatter"
						description="asdf"
						href="/scatter"
					/>
				</Flex>
			</Flex>
		</Box>
	);
};

export default Home;
