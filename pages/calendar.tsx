import { Box, Heading } from '@chakra-ui/react';
import type { NextPage } from 'next';
import { SEO } from '../components/SEO';

const Home: NextPage = () => {
	return (
		<Box m={4}>
			<SEO title="Cakendar" description={undefined} />

			<Heading size="md">TBA</Heading>
		</Box>
	);
};

export default Home;
