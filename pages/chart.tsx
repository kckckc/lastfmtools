import { Box, chakra } from '@chakra-ui/react';
import type { NextPage } from 'next';
import { SEO } from '../components/SEO';

const Home: NextPage = () => {
	return (
		<Box w="100%" m={4}>
			<SEO title="Chart" description={undefined} />

			<chakra.canvas id="chart-canvas" w="800px" h="800px" bg="black" />
		</Box>
	);
};

export default Home;
