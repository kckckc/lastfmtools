# Last.fm tools

## Todo
- When redownloading all data, merge with old results to reduce api calls
- Gracefully handle failed to fetch (incl. 404s)
- Mixed Content error when fetching lastfm api over http from secure app page

## Development
This project is a simple Next.js app: `yarn && yarn dev`
