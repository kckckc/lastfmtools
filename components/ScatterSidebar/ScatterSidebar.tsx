import { Box, Button, Flex, Heading } from '@chakra-ui/react';
import React from 'react';
import { MdRefresh } from 'react-icons/md';
import { trackDataStore } from '../../util/api';
import { generateScatter } from '../../util/generateScatter';

interface ScatterSidebarProps {}

export const ScatterSidebar: React.FC<ScatterSidebarProps> = ({}) => {
	const [trackData] = trackDataStore.hook();

	const handleGenerate = () => {
		generateScatter(trackData.tracks);
	};

	return (
		<Flex direction="column" gap={2}>
			<Flex direction="row" gap={2}>
				<Button
					flex={1}
					leftIcon={<MdRefresh />}
					onClick={handleGenerate}
					size="sm"
					colorScheme="blue"
				>
					Generate
				</Button>
			</Flex>

			<Heading size="sm">Scatterplot options</Heading>

			<Box>asdfg</Box>
			<Box>asdfg</Box>
			<Box>asdfg</Box>
			<Box>asdfg</Box>
		</Flex>
	);
};
