import Head from 'next/head';
import React from 'react';

interface SEOProps {
	title?: string;
	description?: string;
}

const SHARED_TITLE = 'Last.fm Tools';
const DEFAULT_DESCRIPTION = 'Visualizations for Last.fm';

export const SEO: React.FC<SEOProps> = ({ title, description }) => {
	return (
		<Head>
			<title>{(title ? `${title} | ` : '') + SHARED_TITLE}</title>
			<meta
				name="description"
				content={description ?? DEFAULT_DESCRIPTION}
			/>
			<link rel="icon" href="/favicon.ico" />
		</Head>
	);
};
