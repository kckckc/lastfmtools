import { Box, Button, Flex, Heading } from '@chakra-ui/react';
import React, { useState } from 'react';
import { trackDataStore } from '../../util/api';
import { generateChart } from '../../util/generateChart';
import { MdRefresh, MdDownload } from 'react-icons/md';
import { downloadChart } from '../../util/downloadChart';

interface ChartSidebarProps {}

export const ChartSidebar: React.FC<ChartSidebarProps> = ({}) => {
	const [trackData] = trackDataStore.hook();

	const [hasGenerated, setHasGenerated] = useState(false);

	const handleGenerate = () => {
		setHasGenerated(true);

		generateChart(trackData.tracks);
	};

	const handleDownload = () => {
		downloadChart();
	};

	return (
		<Flex direction="column" gap={2}>
			<Flex direction="row" gap={2}>
				<Button
					flex={1}
					leftIcon={<MdRefresh />}
					onClick={handleGenerate}
					size="sm"
					colorScheme="blue"
				>
					{hasGenerated ? 'Update' : 'Generate'}
				</Button>
				<Button
					flex={1}
					leftIcon={<MdDownload />}
					onClick={handleDownload}
					size="sm"
					colorScheme="blue"
					disabled={!hasGenerated}
				>
					Download
				</Button>
			</Flex>

			<Heading size="sm">Chart options</Heading>

			<Box>asdfg</Box>
			<Box>asdfg</Box>
			<Box>asdfg</Box>
			<Box>asdfg</Box>
		</Flex>
	);
};
