import React from 'react';
import NextLink from 'next/link';
import { Box, Heading, Text } from '@chakra-ui/react';

interface LinkCardProps {
	heading: string;
	description: string;
	href: string;
}

export const LinkCard: React.FC<LinkCardProps> = ({
	heading,
	description,
	href,
}) => {
	return (
		<NextLink href={href}>
			<Box
				flex={1}
				w="100%"
				borderWidth={1}
				maxW="400px"
				rounded="lg"
				cursor="pointer"
				px={4}
				py={8}
			>
				<Heading size="lg" mb={2}>
					{heading}
				</Heading>
				<Text>{description}</Text>
			</Box>
		</NextLink>
	);
};
