import { Box, Flex, Heading, Link } from '@chakra-ui/react';
import NextLink from 'next/link';
import React from 'react';
import { SidebarLink } from './SidebarLink';
import { SidebarTools } from './SidebarTools';
import { UserSelector } from './UserSelector';

interface SidebarProps {}

export const Sidebar: React.FC<SidebarProps> = ({}) => {
	return (
		<Flex
			direction="column"
			minH="100vh"
			bg="gray.900"
			borderRightWidth="1px"
			p={4}
			gap={4}
			flexShrink={0}
		>
			<Box>
				<NextLink href="/" passHref>
					<Link>
						<Heading size="md" mb={2}>
							Last.fm Tools
						</Heading>
					</Link>
				</NextLink>
				<Flex direction="row" justifyContent="space-between" gap={2}>
					<SidebarLink to="/chart">Chart</SidebarLink>
					<Box>&#9662;</Box>
					<SidebarLink to="/calendar">Calendar</SidebarLink>
					<Box>&#9662;</Box>
					<SidebarLink to="/scatter">Scatter</SidebarLink>
				</Flex>
			</Box>

			<UserSelector />

			<SidebarTools />

			<Box flex={1} />

			<Box textAlign="center">
				<Box>
					by{' '}
					<Link
						isExternal={true}
						href="https://kckckc.dev"
						color="blue.400"
					>
						kckckc
					</Link>
				</Box>
				<Box>
					<Link
						isExternal={true}
						href="https://gitlab.com/kckckc/lastfmtools"
						color="blue.400"
					>
						view on GitLab
					</Link>
				</Box>
			</Box>
		</Flex>
	);
};
