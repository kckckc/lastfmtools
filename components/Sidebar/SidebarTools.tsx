import { useRouter } from 'next/router';
import React, { ReactNode } from 'react';
import { ChartSidebar } from '../ChartSidebar/ChartSidebar';
import { ScatterSidebar } from '../ScatterSidebar';

interface SidebarToolsProps {}

export const SidebarTools: React.FC<SidebarToolsProps> = ({}) => {
	const router = useRouter();

	let toolsComponent: ReactNode = null;

	if (router.pathname.startsWith('/chart')) {
		toolsComponent = <ChartSidebar />;
	} else if (router.pathname.startsWith('/calendar')) {
		//
	} else if (router.pathname.startsWith('/scatter')) {
		toolsComponent = <ScatterSidebar />;
	}

	return toolsComponent;
};
