import { Link } from '@chakra-ui/react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import React, { ReactNode } from 'react';

interface SidebarLinkProps {
	to: string;
	children?: ReactNode;
}

export const SidebarLink: React.FC<SidebarLinkProps> = ({ to, children }) => {
	const router = useRouter();

	const isActive = router.pathname.startsWith(to);

	return (
		<NextLink href={to} passHref>
			<Link
				fontSize="md"
				textDecoration={isActive ? 'solid underline 1px' : 'none'}
				color="blue.400"
			>
				{children}
			</Link>
		</NextLink>
	);
};
