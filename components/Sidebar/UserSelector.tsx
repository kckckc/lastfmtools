import {
	Box,
	Button,
	Divider,
	Flex,
	Input,
	Progress,
	Spinner,
} from '@chakra-ui/react';
import React, { ReactNode, useEffect } from 'react';
import {
	cachedDataLoadingStore,
	downloadProgress,
	getAllTracks,
	trackDataStore,
} from '../../util/api';
import { useIsHydrated } from '../../util/isHydrated';
import { getInitialUser, useUser } from '../../util/rememberUser';

interface UserSelectorProps {}

export const UserSelector: React.FC<UserSelectorProps> = ({}) => {
	const [username, setUsername] = useUser();

	const [progress] = downloadProgress.hook();
	const [trackData] = trackDataStore.hook();

	const isHydrated = useIsHydrated();

	const [isLoading] = cachedDataLoadingStore.hook();

	// Load initial user
	useEffect(() => {
		if (isHydrated) {
			getInitialUser();
		}
	}, [isHydrated]);

	let progressSection: ReactNode = null;
	if (username.trim() === '') {
		progressSection = null;
	} else if (progress.active) {
		progressSection = (
			<Box w="100%">
				<Progress value={progress.percent * 100} rounded="sm" />
			</Box>
		);
	} else {
		// Either changing name or finished loading data

		progressSection = (
			<Flex w="100%" alignItems="center" justifyContent="center">
				{isLoading ? (
					<Spinner speed="0.6s" />
				) : (
					<>
						<Box>{trackData.tracks.length} scrobbles</Box>
						<Box flex={1} />
						<Button
							size="sm"
							onClick={() => {
								getAllTracks();
							}}
						>
							Refresh
						</Button>
					</>
				)}
			</Flex>
		);
	}

	return (
		<Box>
			<Input
				value={username}
				onChange={(event) => {
					setUsername(event.target.value);
				}}
				placeholder="last.fm username"
				disabled={progress.active}
			/>

			<Flex h={8} my={2} direction="column" justifyContent="center">
				{progressSection}
			</Flex>

			<Divider />
		</Box>
	);
};
