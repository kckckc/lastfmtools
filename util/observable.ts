import { useEffect, useState } from 'react';

type Listener<T> = (data: T) => void;
export class Observable<T> {
	private data: T;
	private listeners: Listener<T>[];

	constructor(value: T) {
		this.data = value;
		this.listeners = [];
	}

	get() {
		return this.data;
	}

	set(value: T) {
		this.data = value;
		this.notify();
	}

	notify() {
		this.listeners.forEach((fn) => fn(this.data));
	}

	subscribe(fn: Listener<T>) {
		this.listeners.push(fn);
	}
	unsubscribe(fn: Listener<T>) {
		this.listeners = this.listeners.filter((f) => f !== fn);
	}

	hook() {
		// eslint-disable-next-line react-hooks/rules-of-hooks
		const [value, setValue] = useState(this.data);

		// eslint-disable-next-line react-hooks/rules-of-hooks
		useEffect(() => {
			const fn = (newValue: T) => {
				setValue(newValue);
			};

			this.subscribe(fn);

			return () => {
				this.unsubscribe(fn);
			};
		}, []);

		return [value, this.set] as const;
	}
}
