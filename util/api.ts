import LZString from 'lz-string';
import { Observable } from './observable';
import { getUser, subscribeUser } from './rememberUser';
import debounce from 'lodash.debounce';

const API_KEY = 'd0ff3f55aa6885275f657162520f59b6';
const API_URL = 'http://ws.audioscrobbler.com/2.0/?';

const CACHE_KEY_PREFIX = 'lastfm-data:';
const API_REQUEST_DELAY = 0; //200;

const LOAD_CACHED_DEBOUNCE = 500;

const FALLBACK_URL =
	'https://lastfm.freetls.fastly.net/i/u/300x300/2a96cbd8b46e442fc41c2b86b821562f.png';
export const IGNORED_IMAGE_URLS = [
	FALLBACK_URL,
	'https://lastfm.freetls.fastly.net/i/u/174s/2a96cbd8b46e442fc41c2b86b821562f.png',
	'https://lastfm.freetls.fastly.net/i/u/64s/2a96cbd8b46e442fc41c2b86b821562f.png',
	'https://lastfm.freetls.fastly.net/i/u/34s/2a96cbd8b46e442fc41c2b86b821562f.png',
];

export interface TrackData {
	album: {
		mbid: string;
		text: string;
	};
	artist: {
		mbid: string;
		text: string;
	};
	date: {
		uts: string;
		text: string;
	};
	image: string;
	mbid: string;
	name: string;
	url: string;
}

const buildRequestUrl = (params: Record<string, string>) => {
	// Set common params
	params.format = 'json';
	params.api_key = API_KEY;

	const queryString = new URLSearchParams(params).toString();
	const requestUrl = API_URL + queryString;

	return requestUrl;
};

export const getNumPages = async () => {
	const url = buildRequestUrl({
		method: 'user.getrecenttracks',
		user: getUser(),
		limit: '1000',
	});

	const data = await fetch(url).then((res) => res.json());
	if (data.error || !data.recenttracks) {
		throw new Error('failed to fetch');
	}

	let result = 0;
	try {
		const totalPages = data.recenttracks['@attr'].totalPages;
		result = parseInt(totalPages);
	} catch {
		throw new Error('failed to fetch');
	}

	return result;
};

const getStorageKey = (user?: string) => {
	return CACHE_KEY_PREFIX + (user ?? getUser());
};

// export const clearCachedTracks = async () => {
// 	localStorage.removeItem(getStorageKey());
// };

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

export const downloadProgress = new Observable({ percent: 0, active: false });

export const trackDataStore = new Observable({
	user: '',
	tracks: [] as TrackData[],
});

const checkCachedData = (user: string) => {
	const cached = localStorage.getItem(getStorageKey());

	if (cached) {
		const json = LZString.decompressFromUTF16(cached);
		if (!json) {
			//?
			throw new Error();
		}
		const data = JSON.parse(json);

		trackDataStore.set(data);
	} else {
		trackDataStore.set({
			user,
			tracks: [],
		});
	}

	cachedDataLoadingStore.set(false);
};
const debouncedCheckCachedData = debounce(
	checkCachedData,
	LOAD_CACHED_DEBOUNCE
);

export const cachedDataLoadingStore = new Observable(false);

// Listen for user changed
subscribeUser((user) => {
	cachedDataLoadingStore.set(true);

	debouncedCheckCachedData(user);
});

export const getAllTracks = async () => {
	const user = getUser().trim();
	if (user === '') return;

	downloadProgress.set({
		percent: 0,
		active: true,
	});

	const numPages = await getNumPages();
	const pages = [];

	for (let i = 0; i < numPages; i++) {
		downloadProgress.set({
			percent: i / numPages,
			active: true,
		});

		console.log('Fetching page ' + (i + 1));
		const pageData = await getTracksPage(user, i + 1);
		pages.push(pageData);

		await sleep(API_REQUEST_DELAY);
	}

	downloadProgress.set({
		percent: 0,
		active: false,
	});

	const finalData = {
		user,
		tracks: pages.flat(),
	};

	trackDataStore.set(finalData);

	compressAndStoreAsync(getStorageKey(user), finalData);
};

// Don't block while doing so
const compressAndStoreAsync = async (key: string, data: any) => {
	const json = JSON.stringify(data);
	const lz = LZString.compressToUTF16(json);

	console.log(
		'Storing ' + lz.length + ' characters after lz-string compression'
	);

	localStorage.setItem(key, lz);
};

const getTracksPage = async (user: string, page: number) => {
	const url = buildRequestUrl({
		method: 'user.getrecenttracks',
		user: user,
		limit: '1000',
		page: page.toString(),
	});

	const data = await fetch(url).then((res) => res.json());
	if (data.error || !data.recenttracks) {
		throw new Error(
			`failed to fetch page ${page}: ` + JSON.stringify(data)
		);
	}

	let result: TrackData[] = [];
	try {
		result = data.recenttracks.track;
	} catch {
		throw new Error(`failed to parse page ${page}`);
	}

	// Transform
	const filtered = result
		.map((track: any) => {
			/* API RESULT:

			interface TrackImageData {
				size: 'small' | 'medium' | 'large' | 'extralarge';
				'#text': string;
			}

			interface TrackData {
				album: {
					mbid: string;
					'#text': string;
				};
				artist: {
					mbid: string;
					'#text': string;
				};
				date: {
					uts: string;
					'#text': string;
				};
				image: TrackImageData[];
				mbid: string;
				name: string;
				streamable: '0' | '1';
				url: string;
			}
			*/

			// Ignore nowplaying
			if (track['@attr']?.nowplaying) {
				return null;
			}

			delete track.streamable;

			let xl = track.image.find((i: any) => i.size === 'extralarge')[
				'#text'
			];
			track.image = xl;

			track.album.text = track.album['#text'];
			delete track.album['#text'];
			track.artist.text = track.artist['#text'];
			delete track.artist['#text'];
			track.date.text = track.date['#text'];
			delete track.date['#text'];

			return track;
		})
		.filter((t) => !!t);

	return filtered;
};

export {};
