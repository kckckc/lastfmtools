import * as d3 from 'd3';
import { differenceInMilliseconds, format, startOfDay } from 'date-fns';
import debounce from 'lodash.debounce';
import { TrackData } from './api';

const tooltipTemplate = `<img src="$IMG"></img>
<div class="scatter-tooltip-details">
<div>$TRACK</div>
<div>$ARTIST</div>
<div>$ALBUM</div>
<div>$DATE</div>
</div>`.replaceAll('\n', '');

export const generateScatter = (tracks: TrackData[]) => {
	const container = document.getElementById('scatter-div');
	if (container) {
		container.innerHTML = '';
	}

	const dataset = tracks.map((t) => {
		// Get date of scrobble
		const date = new Date(parseInt(t.date.uts) * 1000);

		const start = startOfDay(date);
		const time = differenceInMilliseconds(date, start) / (1000 * 60 * 60);

		return {
			...t,
			x: date.getTime(),
			y: time,
			dt: format(date, 'p, PP'),
		};
	});

	const margin = { top: 10, right: 30, bottom: 30, left: 60 };
	const width = 800 - margin.left - margin.right;
	const height = 800 - margin.top - margin.bottom;

	const svg = d3
		.select('#scatter-div')
		.append('svg')
		.attr('width', width + margin.left + margin.right)
		.attr('height', height + margin.top + margin.bottom)
		.append('g')
		.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

	const x = d3
		.scaleTime()
		.domain([
			d3.min(dataset, (t) => t.x - 1)!,
			d3.max(dataset, (t) => t.x + 1)!,
		])
		.range([0, width]);
	const xAxis = svg
		.append('g')
		.attr('transform', 'translate(0,' + height + ')')
		.call(d3.axisBottom(x));

	const y = d3.scaleLinear().domain([0, 24]).range([0, height]);

	const yAxis = svg.append('g').call(
		d3
			.axisLeft(y)
			.tickValues([0, 3, 6, 9, 12, 15, 18, 21, 24])
			.tickFormat((v) => {
				if (v === 0) {
					return '12am';
				} else if (v < 12) {
					return v + 'am';
				} else if (v === 12) {
					return '12pm';
				} else {
					return v.valueOf() - 12 + 'pm';
				}
			})
	);

	// Tooltip
	const tooltip = d3
		.select('body')
		.append('div')
		.attr('class', 'scatter-tooltip')
		.style('opacity', 0)
		.style('transition', 'opacity 0.2s ease')
		.style('position', 'absolute');

	// Zoom
	const zoom = d3.zoom<any, any>().on('zoom', (e) => {
		const newX = e.transform.rescaleX(x);

		xAxis.call(d3.axisBottom(newX));

		updateZoomDebounced(newX);
	});

	const updateZoomDebounced = debounce((newX) => {
		scatter
			.selectAll('circle')
			.attr('cx', (t: any) => newX(t.x))
			.attr('cy', (t: any) => y(t.y));
	}, 300);

	// Zoom container under points
	svg.append('rect')
		.attr('width', width)
		.attr('height', height)
		.style('fill', 'none')
		.style('pointer-events', 'all')
		.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
		.call(zoom);

	// Clip path
	svg.append('defs')
		.append('SVG:clipPath')
		.attr('id', 'clip')
		.append('SVG:rect')
		.attr('width', width)
		.attr('height', height)
		.attr('x', 0)
		.attr('y', 0);

	// Scatter container
	const scatter = svg.append('g').attr('clip-path', 'url(#clip)');

	// Data points
	scatter
		.selectAll('circle')
		.data(dataset)
		.enter()
		.append('circle')
		.attr('cx', (t) => x(t.x))
		.attr('cy', (t) => y(t.y))
		.attr('r', 2)
		.style('fill', '#69b3a2')
		.on('mousemove', (e, t) => {
			tooltip.style('opacity', 0.9);
			tooltip
				.html(
					tooltipTemplate
						.replaceAll('$IMG', t.image)
						.replaceAll('$TRACK', t.name)
						.replaceAll('$ARTIST', t.artist.text)
						.replaceAll('$ALBUM', t.album.text)
						.replaceAll('$DATE', t.dt)
				)
				.style('left', e.pageX - 410 + 'px')
				.style('top', e.pageY - 57 + 'px');
		})
		.on('mouseout', (e, t) => {
			tooltip.transition().duration(500).style('opacity', 0);
		});
};
