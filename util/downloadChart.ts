import { getChartFilename } from './chartOptions';

export const downloadChart = () => {
	const canvas = document.getElementById('chart-canvas');
	if (!canvas || !(canvas instanceof HTMLCanvasElement)) return;

	const el = document.createElement('a');
	el.href = canvas.toDataURL('image/png');
	el.download = getChartFilename();

	el.click();
};
