const cache: Record<string, HTMLImageElement> = {};

const promiseCache: Record<string, Promise<HTMLImageElement>> = {};

export const loadImageFromUrl = async (
	url: string
): Promise<HTMLImageElement> => {
	const cached = cache[url];
	const cachedPromise = promiseCache[url];

	if (cached) {
		return cached;
	} else if (cachedPromise) {
		return await cachedPromise;
	}

	const prom = new Promise<HTMLImageElement>((resolve) => {
		const image = new Image();

		image.crossOrigin = 'anonymous';
		image.src = url;

		image.addEventListener('load', () => {
			cache[url] = image;
			delete promiseCache[url];

			resolve(image);
		});
	});
	promiseCache[url] = prom;

	return prom;
};
