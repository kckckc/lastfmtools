import { useEffect, useState } from 'react';

const USER_KEY = 'last-user';

let currentUser = '';

export const getInitialUser = () => {
	setUser(localStorage.getItem(USER_KEY) ?? '');
};

export const setUser = (value: string) => {
	currentUser = value;
	localStorage.setItem(USER_KEY, value);
	listeners.forEach((fn) => fn(value));
};

export const getUser = () => {
	return currentUser;
};

type UserListener = (user: string) => void;
const listeners: UserListener[] = [];
export const subscribeUser = (fn: UserListener) => {
	listeners.push(fn);
};
export const unsubscribeUser = (fn: UserListener) => {
	const idx = listeners.findIndex((l) => l === fn);
	if (idx > -1) {
		listeners.splice(idx, 1);
	}
};

export const useUser = () => {
	const [value, setValue] = useState(() => {
		return getUser();
	});

	useEffect(() => {
		const fn = (user: string) => {
			setValue(user);
		};

		subscribeUser(fn);

		return () => {
			unsubscribeUser(fn);
		};
	}, []);

	return [value, setUser] as const;
};
