import { IGNORED_IMAGE_URLS, TrackData } from './api';
import { add } from 'date-fns';
import { loadImageFromUrl } from './loadImageFromUrl';

const dayStartsAfter = {
	hours: 6,
};

const getTracksByMonth = (month: number, year: number, tracks: TrackData[]) => {
	const start = add(new Date(year, month, 1), dayStartsAfter);
	const end = add(new Date(year, month + 1, 1), dayStartsAfter);

	const startTime = start.getTime();
	const endTime = end.getTime();

	return tracks.filter((track) => {
		const trackTime = parseInt(track.date.uts) * 1000;

		return trackTime >= startTime && trackTime < endTime;
	});
};

interface AlbumCount {
	id: string;
	count: number;
	images: Record<string, number>;
	topImage: string;
}

export const generateChart = async (tracks: TrackData[]) => {
	const maxAlbums = 25;

	const canvas = document.getElementById('chart-canvas');
	if (!canvas || !(canvas instanceof HTMLCanvasElement)) return;

	const ctx = canvas.getContext('2d');
	if (!ctx) return;

	// Filter
	const filteredTracks = getTracksByMonth(9, 2022, tracks);

	// Count albums
	const albumCounts: Record<string, AlbumCount> = {};
	filteredTracks.forEach((track) => {
		const id = track.album.text;

		if (!albumCounts[id]) {
			albumCounts[id] = {
				id: id,
				count: 0,
				images: {},
				topImage: '',
			};
		}

		albumCounts[id].count += 1;
		albumCounts[id].images[track.image] =
			(albumCounts[id].images[track.image] ?? 0) + 1;
	});

	// Pick most common image
	Object.keys(albumCounts).forEach((k) => {
		const a = albumCounts[k];
		if (Object.keys(a.images).length < 2) {
			a.topImage = Object.keys(a.images)[0];
		} else {
			a.topImage = Object.entries(a.images).sort(
				(a, b) => b[1] - a[1]
			)[0][0];
		}
	});

	// Sort by count and limit
	const albums = Object.values(albumCounts)
		.filter((t) => !IGNORED_IMAGE_URLS.includes(t.topImage)) // Remove ignored images
		.sort((a, b) => b.count - a.count) // Sort by plays
		.slice(0, maxAlbums); // Limit

	// Fetch images
	const imageUrls = albums.map((a) => a.topImage);
	const images = Object.fromEntries(
		await Promise.all(
			imageUrls.map(async (i) => {
				const image = await loadImageFromUrl(i);

				return [i, image];
			})
		)
	);

	const width = 800;
	const height = 800;
	canvas.width = width;
	canvas.height = height;

	// ctx.fillStyle = 'red';
	ctx.clearRect(0, 0, width, height);

	// Draw images
	albums.forEach((album, index) => {
		const img = images[album.topImage];
		const pos = gridLayout(maxAlbums, width, height, index);

		console.log(album.id, album.count, album.topImage);

		ctx.drawImage(img, ...pos);
	});
};

const gridLayout = (
	maxAlbums: number,
	width: number,
	height: number,
	index: number
) => {
	let i = 0;
	while (Math.pow(i, 2) < maxAlbums) {
		i++;
	}

	const x = index % i;
	const y = Math.floor(index / i);

	const s = width / i;

	return [x * s, y * s, s, s] as const;
};
